<?php

/**
 * REQUEST CLASS
 */
class request {

    public static function secureTxt($txt) {
        $txt = htmlentities($txt);
        $txt = stripslashes($txt);
        return $txt;
    }

    public static function securePwd($pwd, $rounds = 9) {

    	$salt = "";
    	$saltChars = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
    	for ($i=0; $i < 22; $i++) {
    		$salt .= $saltChars[array_rand($saltChars)];
    	}
    	return crypt($pwd, sprintf('$2y$%02d$', $rounds) . $salt);

    }

    public static function hashString($string) {
        $string = sha1($string);
        $string = htmlentities($string);
        $string = stripslashes($string);
        return $string;
    }

    public static function trim($string) {
        return trim(preg_replace('/\s+/', ' ', $string));
    }

    public static function randomString() {
        $rand = rand(100000, 999999);
        return self::hashString($rand);
    }

    public static function slug($str) {
        $pieces = explode(" ", $str);
        $first_part = implode(" ", array_splice($pieces, 0, 20));
        $url2 = preg_replace('#[ -]+#', '-', $first_part);
        $url = preg_replace("#[' -]+#", '-', $url2);

        return strtolower($url);
    }

    public static function timeago($time_ago) {

        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds  = $time_elapsed ;
        $minutes  = round($time_elapsed / 60 );
        $hours    = round($time_elapsed / 3600);
        $days     = round($time_elapsed / 86400 );
        $weeks    = round($time_elapsed / 604800);
        $months   = round($time_elapsed / 2600640 );
        $years    = round($time_elapsed / 31207680 );
    // Seconds
        if($seconds <= 60){
            echo "$seconds seconds ago";
        }
    //Minutes
        else if($minutes <=60){
            if($minutes==1){
                echo "one minute ago";
            }
            else{
                echo "$minutes minutes ago";
            }
        }
    //Hours
        else if($hours <=24){
            if($hours==1){
                echo "an hour ago";
            }else{
                echo "$hours hours ago";
            }
        }
    //Days
        else if($days <= 7){
            if($days==1){
                echo "yesterday";
            }else{
                echo "$days days ago";
            }
        }
    //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                echo "a week ago";
            }else{
                echo "$weeks weeks ago";
            }
        }
    //Months
        else if($months <=12){
            if($months==1){
                echo "a month ago";
            }else{
                echo "$months months ago";
            }
        }
    //Years
        else{
            if($years==1){
                echo "one year ago";
            }else{
                echo "$years years ago";
            }
        }

    }// TIME AGO METHOD

    


}
