<?php
session_start();
ob_start();
error_reporting(E_ALL);
require("include/Db.class.php");

// Creates the instance
$db = new Db();

 spl_autoload_register( function($class) {

     if (file_exists("controllers/{$class}.php")) {
         require_once "controllers/{$class}.php";
     }else if (file_exists("classes/{$class}.php")) {
         require_once "classes/{$class}.php";
     }

 });


 $GLOBALS['config'] = array(
     "name" => "SMDP API",
     "domain" => "http://localhost/smdpapp/api/",
     "routes" => array("login", "signup", "polls", "users", "user"),
     "request" => array(
         "method" => $_SERVER['REQUEST_METHOD'],
         "data" => $_REQUEST
         ),
     "path" => array(
         "app" => "app/",
         "core" => "core/",
         "index" => "index"
     ),
     "database" => array(
         "host" => "localhost",
         "user" => "root",
         "password" => "",
         "name" => "smdp"
     )
 );

 new route();
